import 'package:flutter/material.dart';
import '../constants.dart' show Constants, AppColors;

enum ActionItems {
  GROUP_CHAT, ADD_FRIEND, QR_SCAN, PAYMENT
}

class NavigationIconView {
  final BottomNavigationBarItem item;

  NavigationIconView(
      {Key key, String title, IconData icon, IconData activeIcon})
      : item = new BottomNavigationBarItem(
            title: Text(title),
            icon: Icon(icon),
            activeIcon: Icon(activeIcon),
            backgroundColor: Colors.white);
}

class HomeScreen extends StatefulWidget {
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  PageController _pageController;
  int _currentIndex = 0;
  List<NavigationIconView> _navigationViews;
  List<Widget> _pages;

  void initState() {
    super.initState();
    _navigationViews = [
      NavigationIconView(
          title: '微信',
          icon: IconData(0xe61d, fontFamily: Constants.IconFontFamily),
          activeIcon: IconData(0xe61d, fontFamily: Constants.IconFontFamily)),
      NavigationIconView(
          title: '微信',
          icon: IconData(0xe61d, fontFamily: Constants.IconFontFamily),
          activeIcon: IconData(0xe61c, fontFamily: Constants.IconFontFamily)),
      NavigationIconView(
          title: '微信',
          icon: IconData(0xe61d, fontFamily: Constants.IconFontFamily),
          activeIcon: IconData(0xe61c, fontFamily: Constants.IconFontFamily)),
      NavigationIconView(
          title: '微信',
          icon: IconData(0xe61d, fontFamily: Constants.IconFontFamily),
          activeIcon: IconData(0xe61c, fontFamily: Constants.IconFontFamily)),
    ];
    _pageController = PageController(initialPage: _currentIndex);
    _pages = [
      Container(color: Colors.red),
      Container(color: Colors.green),
      Container(color: Colors.blue),
      Container(color: Colors.brown)
    ];
  }

  _buildPopupMenuItem(Widget icon, String title) {
    return Row(
      children: <Widget>[
        icon,
        Container(width: 12.0),
        Text(title, style: TextStyle(color: Color(AppColors.AppBarPopupMenuColor))),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final BottomNavigationBar botNavBar = BottomNavigationBar(
      fixedColor: const Color(AppColors.TabIconActive),
      items: _navigationViews.map((NavigationIconView view) {
        return view.item;
      }).toList(),
      currentIndex: _currentIndex,
      type: BottomNavigationBarType.fixed,
      onTap: (int index) {
        setState(() {
          _currentIndex = index;

          _pageController.animateToPage(_currentIndex, 
            duration: Duration(milliseconds: 200), curve: Curves.easeInOut);
        });
        print('点击的是第 $index 个 Tab');
      },
    );

    return Scaffold(
      appBar: AppBar(
        title: Text('微信'),
        elevation: 0.0,
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              print('点击了搜索按钮');
            },
          ),
          Container(width: 16.0),
//          IconButton(
//            icon: Icon(Icons.add),
//            onPressed: (){ print('显式了下拉列表'); },
//          )
          PopupMenuButton(
              icon: Icon(Icons.add),
              onSelected: (ActionItems selected) {
                  print('点击了 $selected');
              },
              itemBuilder: (BuildContext context) {
                return <PopupMenuItem<ActionItems>>[
                  PopupMenuItem(
                      child: _buildPopupMenuItem(Icon(Icons.add, size: 22.0, color: Color(AppColors.AppBarPopupMenuColor),), '发起群聊'),
                      value: ActionItems.GROUP_CHAT),
                  PopupMenuItem(
                      child: _buildPopupMenuItem(Icon(Icons.add, size: 22.0, color: Color(AppColors.AppBarPopupMenuColor),), '添加朋友'),
                      value: ActionItems.ADD_FRIEND),
                  PopupMenuItem(
                      child: _buildPopupMenuItem(Icon(Icons.add, size: 22.0, color: Color(AppColors.AppBarPopupMenuColor),), '扫一扫'),
                      value: ActionItems.QR_SCAN),
                  PopupMenuItem(
                      child: _buildPopupMenuItem(Icon(Icons.add, size: 22.0, color: Color(AppColors.AppBarPopupMenuColor),), '收付款'),
                      value: ActionItems.PAYMENT),
                ];
              }
          ),
          Container(width: 16.0),
        ],
      ),
      body: PageView.builder(
        itemBuilder: (BuildContext context, int index) {
          return _pages[index];
        },
        controller: _pageController,
        itemCount: _pages.length,
        onPageChanged: (int index) {
          // print('当前显示的是第 $index 页');
          setState(() {
              _currentIndex = index;        
          });
        },
      ),
      bottomNavigationBar: botNavBar,
    );
  }
}

//void _handlePreviousAnimationStatusChanged(AnimationStatus status) {
//  setState(() {
//    if (status == AnimationStatus.dismissed) {
//      assert(_currentController.status == AnimationStatus.dismissed);
//      if (widget.child != null)
//        _currentController.forward();
//    }
//  });
//}
